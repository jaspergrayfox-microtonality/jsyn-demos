package jaspergrayfox.microtonality.utils;

import com.jsyn.JSyn;
import com.jsyn.Synthesizer;
import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.UnitOscillator;
import com.jsyn.unitgen.UnitVoice;
import com.jsyn.util.VoiceAllocator;
import com.jsyn.util.WaveRecorder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Plays audio using JSyn.
 * Only allows for one recording and set of voices at a time.
 */
public class AudioPlayer implements AutoCloseable {
    private final Synthesizer synth;
    private final LineOut lineOut;
    private VoiceAllocator allocator;
    private WaveRecorder recorder;
    private int numVoices;

    /**
     * Initializes an audio player that does not record audio.
     */
    public AudioPlayer() {
        synth = JSyn.createSynthesizer();
        synth.setRealTime(true);
        synth.start();

        lineOut = new LineOut();
        synth.add(lineOut);
    }

    /**
     * Initializes an audio player that records audio to a specified file.
     */
    public AudioPlayer(boolean playLiveAudio, boolean recordAudio, String recordingFileName) {
        synth = JSyn.createSynthesizer();
        synth.setRealTime(playLiveAudio);
        synth.start();

        lineOut = new LineOut();
        synth.add(lineOut);

        if (recordAudio) {
            try {
                recorder = new WaveRecorder(synth, new File(recordingFileName), 2, 24);
                recorder.start();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    // The methods below are arranged in the order that they should be called in.

    /**
     * Adds voices to the mix.
     * All voices must be added at once. Voices cannot be added again later to the same player.
     * @param oscillators The voices to add.
     * @return A VoiceAllocator object that can be used to start playing tunes.
     */
    public VoiceAllocator addVoices(List<UnitOscillator> oscillators) {
        if (allocator != null) {
            throw new IllegalStateException("Voices must all be added at once.");
        }
        numVoices = oscillators.size();
        UnitVoice[] voices = new UnitVoice[numVoices];
        for (int i = 0; i < numVoices; i++) {
            UnitOscillator oscillator = oscillators.get(i);
            oscillator.noteOff();
            synth.add(oscillator);
            voices[i] = oscillator;
            oscillator.output.connect(0, lineOut.input, 0);
            oscillator.output.connect(0, lineOut.input, 1);
            if (recorder != null) {
                oscillator.output.connect(0, recorder.getInput(), 0);
                oscillator.output.connect(0, recorder.getInput(), 1);
            }
        }
        allocator = new VoiceAllocator(voices);
        return allocator;
    }

    public void startPlayback() {
        lineOut.start();
    }

    /**
     * Gets the current time of the synthesizer.
     */
    public double getCurrentSynthTime() {
        return synth.getCurrentTime();
    }

    public void sleepUntil(double time) {
        try {
            synth.sleepUntil(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stopPlayback() {
        if (recorder != null) {
            recorder.stop();
        }
        lineOut.stop();
    }

    public void close() {
        stopPlayback();
        if (recorder != null) {
            try {
                recorder.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        synth.stop();
    }
}
