package jaspergrayfox.microtonality.utils;

import com.jsyn.util.VoiceAllocator;
import com.softsynth.shared.time.TimeStamp;

import java.util.LinkedList;
import java.util.List;

/**
 * A class that plays some chords
 */
public class ChordPlayer {
    private final List<Double> frequencyBuffer;

    public ChordPlayer() {
        frequencyBuffer = new LinkedList<>();
    }

    /**
     * Plays three notes starting at TimeStamp startTime with VoiceAllocator allocator.
     * Returns the new timestamp.
     */
    public TimeStamp playChord3(TimeStamp startTime, VoiceAllocator allocator,
                                double duration, double amplitude,
                                double f1, double f2, double f3) {
        final boolean ENABLE_LOGGING = false;
        TimeStamp endTime = startTime.makeRelative(duration);
        frequencyBuffer.clear();
        frequencyBuffer.add(f1);
        frequencyBuffer.add(f2);
        frequencyBuffer.add(f3);
        if (ENABLE_LOGGING) {
            for (Double frequency : frequencyBuffer) {
                System.out.println("Playing frequency " + frequency);
            }
            System.out.println();
        }
        for (int i = 0; i < 3; i++) {
            allocator.noteOn(i, frequencyBuffer.get(i), amplitude, startTime);
        }
        for (int i = 0; i < 3; i++) {
            allocator.noteOff(i, endTime);
        }
        return endTime;
    }

    /**
     * Plays four notes starting at TimeStamp startTime with VoiceAllocator allocator.
     * Returns the new timestamp.
     */
    public TimeStamp playChord4(TimeStamp startTime, VoiceAllocator allocator,
                                double duration, double amplitude,
                                double f1, double f2, double f3, double f4) {
        final boolean ENABLE_LOGGING = false;
        TimeStamp endTime = startTime.makeRelative(duration);
        frequencyBuffer.clear();
        frequencyBuffer.add(f1);
        frequencyBuffer.add(f2);
        frequencyBuffer.add(f3);
        frequencyBuffer.add(f4);
        if (ENABLE_LOGGING) {
            for (Double frequency : frequencyBuffer) {
                System.out.println("Playing frequency " + frequency);
            }
            System.out.println();
        }
        for (int i = 0; i < 4; i++) {
            allocator.noteOn(i, frequencyBuffer.get(i), amplitude, startTime);
        }
        for (int i = 0; i < 4; i++) {
            allocator.noteOff(i, endTime);
        }
        return endTime;
    }
}
