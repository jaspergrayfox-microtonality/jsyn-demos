package jaspergrayfox.microtonality.utils;

import com.jsyn.unitgen.SineOscillator;
import com.jsyn.unitgen.UnitOscillator;
import com.jsyn.util.VoiceAllocator;
import com.softsynth.shared.time.TimeStamp;

import java.util.LinkedList;
import java.util.List;

/**
 * Tests the AudioPlayer by playing and recording a single sine tone at 220Hz.
 */
public class AudioPlayerPlayToneTest {
    public static void main(String[] args) {
        AudioPlayer player = new AudioPlayer(true, true, "TestTone.wav");

        // Set up the voices
        UnitOscillator oscillator = new SineOscillator();
        oscillator.noteOff();
        List<UnitOscillator> oscillators = new LinkedList<>();
        oscillators.add(oscillator);
        VoiceAllocator allocator = player.addVoices(oscillators);

        // Start playing the tone
        player.startPlayback();
        double currentTime = player.getCurrentSynthTime();
        TimeStamp ts = new TimeStamp(currentTime);
        ts = ts.makeRelative(0.1);
        int tag = 0;
        double freq = 220;
        double amp = 0.1;
        double duration = 1.5;
        allocator.noteOn(tag, freq, amp, ts);
        ts = ts.makeRelative(duration);
        allocator.noteOff(tag, ts);
        player.sleepUntil(ts.getTime() + 0.1);

        // Clean up
        player.stopPlayback();
        player.close();
    }
}
