package jaspergrayfox.microtonality.demos.drifts;

import com.jsyn.unitgen.*;
import com.jsyn.util.VoiceAllocator;
import com.softsynth.shared.time.TimeStamp;
import jaspergrayfox.microtonality.utils.AudioPlayer;
import jaspergrayfox.microtonality.utils.ChordPlayer;

import java.util.LinkedList;
import java.util.List;

/**
 * Generates multiple audio files of the Neo-Riemannian progression
 * Cm Eb Bbm F
 * repeated several times.
 * The progression results in an upwards syntonic comma pump.
 * Each audio file is in a different EDO, and a JI file is also produced for reference.
 * EDOs to try: 12, 41, 53, 72.
 */
public class AscendingSyntonicCommaDemo {
    private final ChordPlayer player;

    public AscendingSyntonicCommaDemo() {
        player = new ChordPlayer();
    }

    private TimeStamp playProgression(TimeStamp startTime, VoiceAllocator allocator,
                                      double perfectFifthGenerator, double majorThirdGenerator) {
        final double DEFAULT_NOTE_AMPLITUDE = 0.2 / 4;
        final double DEFAULT_NOTE_DURATION = 1.5;
        final int CYCLES = 8;

        TimeStamp ts = startTime;

        double a3Freq = 220;
        double minorThirdGenerator = perfectFifthGenerator / majorThirdGenerator;
        double c4Freq = a3Freq * minorThirdGenerator;
        double baseFreq = c4Freq;

        for (int i = 0; i < CYCLES; i++) {
            // C minor chord
            // C3 C4 Eb4 G4
            // baseFreq = C4 at this point
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq,
                    baseFreq * minorThirdGenerator,
                    baseFreq * perfectFifthGenerator);

            // E-flat major chord
            // G2 Bb3 Eb4 Bb4
            // baseFreq = Eb4
            baseFreq = baseFreq * minorThirdGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq * majorThirdGenerator / 4,
                    baseFreq * perfectFifthGenerator / 2,
                    baseFreq,
                    baseFreq * perfectFifthGenerator);

            // B-flat minor chord
            // Bb2 Db4 F4 Bb4
            // baseFreq = Bb3
            baseFreq = baseFreq * perfectFifthGenerator / 2;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq * minorThirdGenerator,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2);

            // F major chord
            // F2 C4 F4 A4
            // baseFreq = F4
            baseFreq = baseFreq * perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 4,
                    baseFreq * perfectFifthGenerator / 2,
                    baseFreq,
                    baseFreq * majorThirdGenerator);

            // baseFreq = C4
            baseFreq = baseFreq * perfectFifthGenerator / 2;
        }

        return ts;
    }

    private void createRecording(String filename,
                                 double perfectFifthGenerator, double majorThirdGenerator) {
        final int NUM_VOICES = 4;
        AudioPlayer player = new AudioPlayer(false, true, filename);

        List<UnitOscillator> oscillators = new LinkedList<>();
        for (int i = 0; i < NUM_VOICES; i++) {
            UnitOscillator osc = new SawtoothOscillatorBL();
            oscillators.add(osc);
        }
        VoiceAllocator allocator = player.addVoices(oscillators);

        // Play the progression.
        // Advance to a near future time, so we have a clean start.
        player.startPlayback();
        double timeNow = player.getCurrentSynthTime();
        TimeStamp ts = new TimeStamp(timeNow + 0.25);
        ts = playProgression(ts, allocator, perfectFifthGenerator, majorThirdGenerator);

        player.sleepUntil(ts.getTime() + 0.25);
        player.stopPlayback();
        player.close();
    }

    public static void main(String[] args) {
        AscendingSyntonicCommaDemo demo = new AscendingSyntonicCommaDemo();

        // List of EDOs (equal divisions of the octave) we want to demo.
        int[] edosToDemo = { 12, 41, 53, 72 };
        // List perfect fifth generators and major third generators for the respective EDOs.
        int[] perfectFifthIndices = { 7, 24, 31, 42 };
        int[] majorThirdIndices = { 4, 13, 17, 23 };
        // Run the demo for the EDOs above.
        for (int i = 0; i < edosToDemo.length; i++) {
            int edo = edosToDemo[i];
            int perfectFifthIndex = perfectFifthIndices[i];
            int majorThirdIndex = majorThirdIndices[i];
            demo.createRecording(String.format("AscendingSyntonicCommaDemo_%dEDO.wav", edo),
                    Math.pow(2, perfectFifthIndex / (double)edo),
                    Math.pow(2, majorThirdIndex / (double)edo));
        }

        // Also run the demo once for JI (just intonation).
        demo.createRecording("AscendingSyntonicCommaDemo_JI.wav", 3 / 2.0, 5 / 4.0);
    }
}
