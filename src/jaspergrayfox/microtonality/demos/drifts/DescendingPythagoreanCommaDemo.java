package jaspergrayfox.microtonality.demos.drifts;

import com.jsyn.unitgen.*;
import com.jsyn.util.VoiceAllocator;
import com.softsynth.shared.time.TimeStamp;
import jaspergrayfox.microtonality.utils.AudioPlayer;
import jaspergrayfox.microtonality.utils.ChordPlayer;

import java.util.LinkedList;
import java.util.List;

/**
 * Generates multiple audio files of a descending fifths progression repeated several times.
 * The progression results in a downwards Pythagorean comma pump.
 * Each audio file is in a different EDO, and a JI file is also produced for reference.
 * EDOs to try: 12, 53.
 */
public class DescendingPythagoreanCommaDemo {
    private final ChordPlayer player;

    public DescendingPythagoreanCommaDemo() {
        player = new ChordPlayer();
    }

    private TimeStamp playProgression(TimeStamp startTime, VoiceAllocator allocator,
                                      double perfectFifthGenerator,
                                      double majorThirdGenerator) {
        final double DEFAULT_NOTE_AMPLITUDE = 0.2 / 4;
        final double DEFAULT_NOTE_DURATION = 0.75;
        final int CYCLES = 8;

        TimeStamp ts = startTime;

        double a3Freq = 220;
        double minorThirdGenerator = perfectFifthGenerator / majorThirdGenerator;
        double c4Freq = a3Freq * minorThirdGenerator;
        double baseFreq = c4Freq;

        for (int i = 0; i < CYCLES; i++) {
            // Cm, C3 G3 Eb4 C5, baseFreq = C4
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq / 2 * perfectFifthGenerator,
                    baseFreq * minorThirdGenerator,
                    baseFreq * 2);
            // C, Eb4 -> E4
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq / 2 * perfectFifthGenerator,
                    baseFreq * majorThirdGenerator,
                    baseFreq * 2);

            // Fm, F2 Ab3 F4 C5, baseFreq = F3
            baseFreq = baseFreq / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq / 2 * minorThirdGenerator,
                    baseFreq * 2,
                    baseFreq * 2 * perfectFifthGenerator);
            // F, Ab3 -> A3
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq / 2 * majorThirdGenerator,
                    baseFreq * 2,
                    baseFreq * 2 * perfectFifthGenerator);

            // Bbm, Bb2 Bb3 F4 Db5, baseFreq = Bb3
            baseFreq = baseFreq * 2 / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2 * minorThirdGenerator);
            // Bb, Db5 -> D5
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2 * majorThirdGenerator);

            // Ebm, Eb2 Bb3 Gb4 Eb5, baseFreq = Eb3
            baseFreq = baseFreq / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2 * minorThirdGenerator,
                    baseFreq * 4);
            // Eb, Gb4 -> G4
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2 * majorThirdGenerator,
                    baseFreq * 4);

            // Abm, Ab2 Cb4 Ab4 Eb5, baseFreq = Ab3
            baseFreq = baseFreq * 2 / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq / 2 * minorThirdGenerator,
                    baseFreq * 2,
                    baseFreq * 2 * perfectFifthGenerator);
            // Ab, Cb4 -> C4
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq / 2 * majorThirdGenerator,
                    baseFreq * 2,
                    baseFreq * 2 * perfectFifthGenerator);

            // Dbm, Db3 Db4 Ab4 Fbb5, baseFreq = Db4
            baseFreq = baseFreq * 2 / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2 * minorThirdGenerator);
            // Db, Fbb5 -> Fb5
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2 * majorThirdGenerator);

            // Gbm, Gb2 Db4 Bbb4 Gb5, baseFreq = Gb4
            baseFreq = baseFreq * 2 / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 4,
                    baseFreq / 2 * perfectFifthGenerator,
                    baseFreq * minorThirdGenerator,
                    baseFreq * 2);
            // Gb, Gb2 Db4 Bb4 Gb5 in 12edo is enharmonic with
            // F#, F#2 C#4 A#4 F#
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 4,
                    baseFreq / 2 * perfectFifthGenerator,
                    baseFreq * majorThirdGenerator,
                    baseFreq * 2);

            // Bm, B2 B3 B4 D5, baseFreq = B3
            baseFreq = baseFreq / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq,
                    baseFreq * 2,
                    baseFreq * 2 * minorThirdGenerator);
            // B,  B2 F#3 B4 D#5
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq / 2 * perfectFifthGenerator,
                    baseFreq * 2,
                    baseFreq * 2 * majorThirdGenerator);

            // Em, E2 G3 B4 E5, baseFreq = E4
            baseFreq = baseFreq * 2 / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 4,
                    baseFreq / 2 * minorThirdGenerator,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2);
            // E7, E2 G#3 B4 E5
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 4,
                    baseFreq / 2 * majorThirdGenerator,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2);

            // Am, A2 A3 A4 C5, baseFreq = A3
            baseFreq = baseFreq / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq,
                    baseFreq * 2,
                    baseFreq * 2 * minorThirdGenerator);
            // A,  A2 E3 A4 C#5
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq / 2 * perfectFifthGenerator,
                    baseFreq * 2,
                    baseFreq * 2 * majorThirdGenerator);

            // Dm, D3 F3 A4 D5, baseFreq = D4
            baseFreq = baseFreq * 2 / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq / 2 * minorThirdGenerator,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2);
            // D,  D3 F#3 A4 D5
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq / 2 * majorThirdGenerator,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2);

            // Gm, G2 G3 G4 Bb4, baseFreq = G3
            baseFreq = baseFreq / perfectFifthGenerator;
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq,
                    baseFreq * 2,
                    baseFreq * 2 * minorThirdGenerator);
            // G,  G2 G3 D4 B4
            ts = player.playChord4(ts, allocator, DEFAULT_NOTE_DURATION, DEFAULT_NOTE_AMPLITUDE,
                    baseFreq / 2,
                    baseFreq,
                    baseFreq * perfectFifthGenerator,
                    baseFreq * 2 * majorThirdGenerator);

            // Cm, C3 G3 Eb4 C5, baseFreq = C4
            baseFreq = baseFreq * 2 / perfectFifthGenerator;
        }

        return ts;
    }

    private void createRecording(String filename,
                                 double perfectFifthGenerator,
                                 double majorThirdGenerator) {
        final int NUM_VOICES = 4;
        AudioPlayer player = new AudioPlayer(false, true, filename);

        List<UnitOscillator> oscillators = new LinkedList<>();
        for (int i = 0; i < NUM_VOICES; i++) {
            UnitOscillator osc = new SquareOscillatorBL();
            oscillators.add(osc);
        }
        VoiceAllocator allocator = player.addVoices(oscillators);

        // Play the progression.
        // Advance to a near future time, so we have a clean start.
        player.startPlayback();
        double timeNow = player.getCurrentSynthTime();
        TimeStamp ts = new TimeStamp(timeNow + 0.25);
        ts = playProgression(ts, allocator, perfectFifthGenerator, majorThirdGenerator);

        player.sleepUntil(ts.getTime() + 0.25);
        player.stopPlayback();
        player.close();
    }

    public static void main(String[] args) {
        DescendingPythagoreanCommaDemo demo = new DescendingPythagoreanCommaDemo();

        // List of EDOs (equal divisions of the octave) we want to demo.
        int[] edosToDemo = { 12, 53, 72 };
        // List generators for the respective EDOs.
        int[] perfectFifthIndices = { 7, 31, 42 };
        int[] majorThirdIndices = { 4, 17, 23 };
        // Run the demo for the EDOs above.
        for (int i = 0; i < edosToDemo.length; i++) {
            int edo = edosToDemo[i];
            int perfectFifthIndex = perfectFifthIndices[i];
            int majorThirdIndex = majorThirdIndices[i];
            demo.createRecording(String.format("DescendingPythagoreanCommaDemo_%dEDO.wav", edo),
                    Math.pow(2, perfectFifthIndex / (double)edo),
                    Math.pow(2, majorThirdIndex / (double)edo));
        }

        // Also run the demo once for JI (just intonation).
        demo.createRecording("DescendingPythagoreanCommaDemo_JI.wav", 3 / 2.0, 5 / 4.0);
    }
}
