package jaspergrayfox.microtonality.demos.ji;

import com.jsyn.unitgen.*;
import com.jsyn.util.VoiceAllocator;
import com.softsynth.shared.time.TimeStamp;
import jaspergrayfox.microtonality.utils.AudioPlayer;
import jaspergrayfox.microtonality.utils.ChordPlayer;

import java.util.LinkedList;
import java.util.List;

/**
 * Various chords of the form 1, X, 3/2 where X is a third such as 5/4, 7/6, etc.
 * Either solid or broken.
 */
public class ThirdChordsDemo {
    private final ChordPlayer player;

    public ThirdChordsDemo() {
        player = new ChordPlayer();
    }

    private TimeStamp playProgression(TimeStamp ts, VoiceAllocator allocator) {
        final double A4_FREQ = 440;
        final double NOTE_DURATION = 2.5;
        final double GAP_DURATION = 1;
        final double NOTE_AMPLITUDE = 0.3 / 4;

        final double PERFECT_FIFTH = 3.0 / 2;
        // final double OCTAVE = 2;
        List<Double> thirds = new LinkedList<>();

        // Numbers represent the ratio between the two frequencies
        // 7-limit subminor third
        // 1.1666666666666667
        thirds.add(7.0 / 6);
        // 13-limit minor third
        // 1.1818181818181819
        thirds.add(13.0 / 11);
        // 3-limit minor third
        // 1.1851851851851851
        thirds.add(32.0 / 27);
        // 12edo minor third
        // 1.189207115002721
        // thirds.add(Math.pow(2, 3.0 / 12));
        // 5-limit minor third
        // 1.2
        thirds.add(6.0 / 5);

        // 13-limit lesser neutral third
        // 1.21875
        thirds.add(39.0 / 32);
        // 11-limit lesser neutral third
        // 1.2222222222222223
        thirds.add(11.0 / 9);
        // 11-limit greater neutral third (Rastmic neutral third)
        // 1.2272727272727273
        thirds.add(27.0 / 22);
        // 13-limit greater neutral third
        // 1.2307692307692308
        thirds.add(16.0 / 13);

        // 5-limit major third
        // 1.25
        thirds.add(5.0 / 4);
        // 12edo major third
        // 1.2599210498948732
        // thirds.add(Math.pow(2, 4.0 / 12));
        // 3-limit major third
        // 1.265625
        thirds.add(81.0 / 64);
        // 13-limit major third
        // 1.2692307692307692
        thirds.add(33.0 / 26);
        // 7-limit supermajor third
        // 1.2857142857142858
        thirds.add(9.0 / 7);

        double baseFreq = A4_FREQ;
        for (Double third : thirds) {
            ts = player.playChord3(ts, allocator,
                NOTE_DURATION, NOTE_AMPLITUDE,
                baseFreq,
                baseFreq * third,
                baseFreq * PERFECT_FIFTH);
            ts = ts.makeRelative(GAP_DURATION);
        }

        return ts;
    }

    private void createRecording(String filename) {
        final int NUM_VOICES = 3;
        AudioPlayer player = new AudioPlayer(false, true, filename);

        List<UnitOscillator> oscillators = new LinkedList<>();
        for (int i = 0; i < NUM_VOICES; i++) {
            UnitOscillator osc = new TriangleOscillator();
            osc.noteOff();
            oscillators.add(osc);
        }
        VoiceAllocator allocator = player.addVoices(oscillators);

        // Play the progression.
        // Advance to a near future time, so we have a clean start.
        player.startPlayback();
        double timeNow = player.getCurrentSynthTime();
        TimeStamp ts = new TimeStamp(timeNow + 0.25);

        ts = playProgression(ts, allocator);

        player.sleepUntil(ts.getTime() + 0.25);
        player.stopPlayback();
        player.close();
    }

    public static void main(String[] args) {
        ThirdChordsDemo demo = new ThirdChordsDemo();
        demo.createRecording("ThirdChordsDemo.wav");
    }
}
