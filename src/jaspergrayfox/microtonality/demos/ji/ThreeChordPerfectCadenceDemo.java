package jaspergrayfox.microtonality.demos.ji;

import com.jsyn.unitgen.*;
import com.jsyn.util.VoiceAllocator;
import com.softsynth.shared.time.TimeStamp;
import jaspergrayfox.microtonality.utils.AudioPlayer;
import jaspergrayfox.microtonality.utils.ChordPlayer;

import java.util.LinkedList;
import java.util.List;

/**
 * A I64-V7-I progression in 7-limit JI.
 */
public class ThreeChordPerfectCadenceDemo {
    private final ChordPlayer player;

    public ThreeChordPerfectCadenceDemo() {
        player = new ChordPlayer();
    }

    private TimeStamp playProgression(TimeStamp ts, VoiceAllocator allocator) {
        final double A4_FREQ = 440;
        final double NOTE_DURATION = 1.5;
        final double NOTE_AMPLITUDE = 0.3 / 4;

        final double MAJOR_THIRD = 5.0 / 4;
        final double PERFECT_FIFTH = 3.0 / 2;
        final double HARMONIC_SEVENTH = 7.0 / 4;
        final double OCTAVE = 2;

        // Play I64
        // G E C G
        double baseFreq = A4_FREQ * 6.0 / 5 / 2; // C4
        ts = player.playChord4(ts, allocator,
                NOTE_DURATION, NOTE_AMPLITUDE,
                baseFreq * PERFECT_FIFTH / OCTAVE,
                baseFreq * MAJOR_THIRD,
                baseFreq * OCTAVE,
                baseFreq * OCTAVE * PERFECT_FIFTH);

        // Play V7
        // G D B F
        baseFreq = baseFreq * PERFECT_FIFTH; // G4
        ts = player.playChord4(ts, allocator,
                NOTE_DURATION, NOTE_AMPLITUDE,
                baseFreq / OCTAVE,
                baseFreq / OCTAVE * PERFECT_FIFTH,
                baseFreq * MAJOR_THIRD,
                baseFreq * HARMONIC_SEVENTH);

        // Play I
        // C C C E
        baseFreq = baseFreq / PERFECT_FIFTH; // C4
        ts = player.playChord4(ts, allocator,
                NOTE_DURATION * 2, NOTE_AMPLITUDE,
                baseFreq / OCTAVE,
                baseFreq,
                baseFreq * OCTAVE,
                baseFreq * OCTAVE * MAJOR_THIRD);

        return ts;
    }

    private void createRecording(String filename) {
        final int NUM_VOICES = 4;
        AudioPlayer player = new AudioPlayer(true, false, filename);

        List<UnitOscillator> oscillators = new LinkedList<>();
        for (int i = 0; i < NUM_VOICES; i++) {
            UnitOscillator osc = new TriangleOscillator();
            osc.noteOff();
            oscillators.add(osc);
        }
        VoiceAllocator allocator = player.addVoices(oscillators);

        // Play the progression.
        // Advance to a near future time, so we have a clean start.
        player.startPlayback();
        double timeNow = player.getCurrentSynthTime();
        TimeStamp ts = new TimeStamp(timeNow + 0.25);

        ts = playProgression(ts, allocator);

        player.sleepUntil(ts.getTime() + 0.25);
        player.stopPlayback();
        player.close();
    }

    public static void main(String[] args) {
        ThreeChordPerfectCadenceDemo demo = new ThreeChordPerfectCadenceDemo();
        demo.createRecording("ThreeChordCadence54.wav");
    }
}
