package jaspergrayfox.microtonality.demos.firststeps;

import com.jsyn.JSyn;
import com.jsyn.Synthesizer;
import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SawtoothOscillator;
import com.jsyn.unitgen.UnitOscillator;
import com.softsynth.shared.time.TimeStamp;

public class SingleNoteDemo {
    public void play() {
        // Set up the synthesizer.
        Synthesizer synth = JSyn.createSynthesizer();
        synth.start();

        // Set up the oscillator and output.
        UnitOscillator osc = new SawtoothOscillator();
        osc.noteOff(); // Otherwise we risk hearing a loud A440.
        LineOut lineOut = new LineOut();
        synth.add(osc);
        synth.add(lineOut);
        osc.output.connect(0, lineOut.input, 0);
        osc.output.connect(0, lineOut.input, 1);

        // Get synthesizer time in seconds.
        double timeNow = synth.getCurrentTime();
        // Advance to a near future time so we have a clean start.
        double time = timeNow + 1.0;
        TimeStamp ts = new TimeStamp(time);

        // Start the output.
        lineOut.start();
        // Alternatively:
        // synth.startUnit(lineOut, ts);

        // Add a note
        double freq = 220;
        double amp = 0.15;
        double duration = 1;
        osc.noteOn(freq, amp, ts);
        osc.noteOff(ts.makeRelative(duration));

        try {
            double sleepLength = 2;
            synth.sleepUntil(sleepLength);
        } catch (InterruptedException e) {
            System.err.println("Interrupted");
        }
        synth.stop();
    }

    public static void main(String[] args) {
        (new SingleNoteDemo()).play();
    }
}
