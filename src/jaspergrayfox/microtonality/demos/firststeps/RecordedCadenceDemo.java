package jaspergrayfox.microtonality.demos.firststeps;

import com.jsyn.JSyn;
import com.jsyn.Synthesizer;
import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SawtoothOscillator;
import com.jsyn.unitgen.UnitOscillator;
import com.jsyn.unitgen.UnitVoice;
import com.jsyn.util.VoiceAllocator;
import com.jsyn.util.WaveRecorder;
import com.softsynth.shared.time.TimeStamp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class RecordedCadenceDemo {
    final double NOTE_DURATION = 2;
    // If we don't allocate enough voices, we get an NPE
    final int VOICES = 4;
    final double NOTE_AMP = 0.15 / VOICES;

    final boolean ENABLE_LOGGING = false;
    final boolean ENABLE_RECORDING = false;
    final boolean ENABLE_LIVE_OUTPUT = true;

    /**
     * Plays each note for one second with oscillator osc, starting at timestamp ts.
     * Returns the new timestamp.
      */
    private TimeStamp playChord(TimeStamp ts, VoiceAllocator allocator, List<Double> frequencies) {
        TimeStamp endTime = ts.makeRelative(NOTE_DURATION);

        if (ENABLE_LOGGING) {
            System.out.println("Playing " + frequencies.size() + " notes");
            for (double frequency : frequencies) {
                System.out.println("Playing frequency " + frequency + "Hz");
            }
            System.out.println();
        }

        for (int i = 0; i < frequencies.size(); i++) {
            allocator.noteOn(i, frequencies.get(i), NOTE_AMP, ts);
        }
        for (int i = 0; i < frequencies.size(); i++) {
            allocator.noteOff(i, endTime);
        }
        return endTime;
    }

    public void play() {
        // Set up the synthesizer.
        Synthesizer synth = JSyn.createSynthesizer();
        synth.setRealTime(ENABLE_LIVE_OUTPUT);
        synth.start();

        // Set up the external audio output device.
        LineOut lineOut = new LineOut();
        synth.add(lineOut);

        // Set up the recorder.
        WaveRecorder recorder;
        if (ENABLE_RECORDING) {
            try {
                recorder = new WaveRecorder(synth, new File("recorded_cadence.wav"), 2, 24);
            } catch (FileNotFoundException e) {
                System.err.println("Unable to initialize output WAV file.");
                synth.stop();
                return;
            }
        }

        // Set up the voice allocator and oscillators.
        // For every single voice we need an oscillator.
        UnitVoice[] voices = new UnitVoice[4];
        for (int i = 0; i < VOICES; i++) {
            UnitOscillator osc = new SawtoothOscillator();
            osc.noteOff();
            synth.add(osc);
            osc.output.connect(0, lineOut.input, 0);
            osc.output.connect(0, lineOut.input, 1);
            voices[i] = osc;
            if (ENABLE_RECORDING) {
                osc.output.connect(0, recorder.getInput(), 0);
                osc.output.connect(0, recorder.getInput(), 1);
            }
        }
        VoiceAllocator allocator = new VoiceAllocator(voices);

        // Get synthesizer time in seconds.
        double timeNow = synth.getCurrentTime();
        // Advance to a near future time so we have a clean start.
        double time = timeNow + 0.5;
        TimeStamp ts = new TimeStamp(time);

        if (ENABLE_RECORDING) {
            recorder.start();
        }

        // Start the output.
        synth.startUnit(lineOut, ts);

        // Play notes.
        double tonicBaseFreq = 220;
        double dominantBaseFreq = tonicBaseFreq * 3.0/2;
        List<Double> frequencies = new LinkedList<>();
        frequencies.add(dominantBaseFreq);
        frequencies.add(dominantBaseFreq * 5.0/4);
        frequencies.add(dominantBaseFreq * 3.0/2);
        frequencies.add(dominantBaseFreq * 7.0/4);
        ts = playChord(ts, allocator, frequencies);

        frequencies.clear();
        frequencies.add(tonicBaseFreq);
        frequencies.add(tonicBaseFreq * 2.0);
        frequencies.add(tonicBaseFreq * 5.0/2);
        ts = playChord(ts, allocator, frequencies);

        try {
            synth.sleepUntil(ts.getTime());
        } catch (InterruptedException e) {
            System.err.println("Interrupted");
        }

        if (ENABLE_RECORDING) {
            recorder.stop();
            try {
                recorder.close();
            } catch (IOException e) {
                System.err.println("Recorder failed. Printing error message:");
                System.err.println(e.getMessage());
            }
        }
        synth.stop();
    }

    public static void main(String[] args) {
        (new RecordedCadenceDemo()).play();
    }
}
