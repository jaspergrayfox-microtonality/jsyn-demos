package jaspergrayfox.microtonality.demos.firststeps;

import com.jsyn.JSyn;
import com.jsyn.Synthesizer;
import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SawtoothOscillator;
import com.jsyn.unitgen.UnitOscillator;
import com.jsyn.unitgen.UnitVoice;
import com.jsyn.util.VoiceAllocator;
import com.softsynth.shared.time.TimeStamp;

import java.util.LinkedList;
import java.util.List;

public class SolidChordDemo {
    final double NOTE_DURATION = 2;
    // If we don't allocate enough voices, we get an NPE.
    final int VOICES = 4;
    final double NOTE_AMP = 0.15 / VOICES;

    /**
     * Plays each note for one second with oscillator osc, starting at timestamp ts.
     * Returns the new timestamp.
      */
    private TimeStamp playChord(TimeStamp ts, VoiceAllocator allocator, List<Double> frequencies) {
        TimeStamp endTime = ts.makeRelative(NOTE_DURATION);
        for (int i = 0; i < frequencies.size(); i++) {
            allocator.noteOn(i, frequencies.get(i), NOTE_AMP, ts);
        }
        for (int i = 0; i < frequencies.size(); i++) {
            allocator.noteOff(i, endTime);
        }
        return endTime;
    }

    public void play() {
        // Set up the synthesizer.
        Synthesizer synth = JSyn.createSynthesizer();
        synth.start();

        // Set up the external audio output device.
        LineOut lineOut = new LineOut();
        synth.add(lineOut);

        // Set up the voice allocator and oscillators.
        // For every single voice we need an oscillator.
        UnitVoice[] voices = new UnitVoice[4];
        for (int i = 0; i < VOICES; i++) {
            UnitOscillator osc = new SawtoothOscillator();
            osc.noteOff(); // Protect against loud A440
            synth.add(osc);
            osc.output.connect(0, lineOut.input, 0);
            osc.output.connect(0, lineOut.input, 1);
            voices[i] = osc;
        }
        VoiceAllocator allocator = new VoiceAllocator(voices);

        // Get synthesizer time in seconds.
        double timeNow = synth.getCurrentTime();
        // Advance to a near future time so we have a clean start.
        double time = timeNow + 1.0;
        TimeStamp ts = new TimeStamp(time);

        // Start the output.
        synth.startUnit(lineOut, ts);

        // Play notes.
        double baseFrequency = 220;
        List<Double> frequencies = new LinkedList<>();
        frequencies.add(baseFrequency);
        frequencies.add(baseFrequency * 5.0/4);
        frequencies.add(baseFrequency * 3.0/2);
        frequencies.add(baseFrequency * 7.0/4);
        ts = playChord(ts, allocator, frequencies);

        try {
            synth.sleepUntil(ts.getTime());
        } catch (InterruptedException e) {
            System.err.println("Interrupted");
        }
        synth.stop();
    }

    public static void main(String[] args) {
        (new SolidChordDemo()).play();
    }
}
