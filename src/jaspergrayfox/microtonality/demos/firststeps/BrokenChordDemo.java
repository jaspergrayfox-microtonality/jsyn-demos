package jaspergrayfox.microtonality.demos.firststeps;

import com.jsyn.JSyn;
import com.jsyn.Synthesizer;
import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SawtoothOscillator;
import com.jsyn.unitgen.UnitOscillator;
import com.softsynth.shared.time.TimeStamp;

import java.util.LinkedList;
import java.util.List;

public class BrokenChordDemo {
    final double NOTE_AMP = 0.15;
    final double NOTE_DURATION = 0.5;

    /**
     * Plays each note for one second with oscillator osc, starting at timestamp ts.
     * Returns the new timestamp.
      */
    private TimeStamp playNotes(TimeStamp ts, UnitOscillator osc, List<Double> frequencies) {
        for (double freq : frequencies) {
            osc.noteOn(freq, NOTE_AMP, ts);
            ts = ts.makeRelative(NOTE_DURATION);
            osc.noteOff(ts);
        }
        return ts;
    }

    public void play() {
        // Set up the synthesizer.
        Synthesizer synth = JSyn.createSynthesizer();
        synth.start();

        // Set up the oscillator and output.
        UnitOscillator osc = new SawtoothOscillator();
        osc.noteOff(); // Protect against loud A440
        LineOut lineOut = new LineOut();
        synth.add(osc);
        synth.add(lineOut);
        osc.output.connect(0, lineOut.input, 0);
        osc.output.connect(0, lineOut.input, 1);

        // Get synthesizer time in seconds.
        double timeNow = synth.getCurrentTime();
        // Advance to a near future time so we have a clean start.
        double time = timeNow + 1.0;
        TimeStamp ts = new TimeStamp(time);

        // Start the output.
        synth.startUnit(lineOut, ts);

        // Play notes.
        double baseFrequency = 220;
        List<Double> frequencies = new LinkedList<>();
        frequencies.add(baseFrequency);
        frequencies.add(baseFrequency * 5.0/4);
        frequencies.add(baseFrequency * 3.0/2);
        frequencies.add(baseFrequency * 7.0/4);
        frequencies.add(baseFrequency * 2.0);
        ts = playNotes(ts, osc, frequencies);

        try {
            synth.sleepUntil(ts.getTime());
        } catch (InterruptedException e) {
            System.err.println("Interrupted");
        }
        synth.stop();
    }

    public static void main(String[] args) {
        (new BrokenChordDemo()).play();
    }
}
