# jsyn-demos

This repository contains several demos that use JSyn to produce audio samples.

The `firststeps` package contains some starter files that show how to generate
tones and record them into a WAV file.

The `drifts` package contains demos that play and/or record
progressions that start drifting in certain tunings due to comma pumps.

To learn more about comma pumps, watch 
[Adam Neely's video on YouTube](https://www.youtube.com/watch?v=TYhPAbsIqA8)
or read about them on the [Xenharmonic Wiki](https://en.xen.wiki/w/Comma_pump).

## Notice of licensing

This repository is licensed under the MIT License.

However, the files under the `lib` directory are from JSyn, 
whose source code is licensed under the Apache 2.0 License.
See https://github.com/philburk/jsyn/blob/master/NOTICE.txt for more details.
